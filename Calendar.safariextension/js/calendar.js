var now = new Date(); //Today
var currentYear = now.getUTCFullYear(); //This year, now with more UTC
var currentMonth = now.getUTCMonth() + 1;

var normalClass = "cal_td_day"; //The class for a normal day
var selectedClass = "cal_day_chosen"; //God knows
var selectedSubClass = "cal_subday_chosen";
var dynamicClass = "cal_day_dynamic";

var yearButtonFullClass = "thisyear";
var yearButtonHalfClass = "thisyearhalf";
var yearButtonThirdClass = "thisyearthird";

var daysSelectString = "."+normalClass+",."+selectedClass+",."+selectedSubClass;

var dynamicStartStamp = false;
var dynamicStopStamp = false;
var dynamicDiff = false;

var lastEventDate = "";

var showFromStart; 

var showingFromMonth;
var showingFromYear; 

var runningMouseWheelCounter = 0;
var runningMouseWheelTimer;

var currentTodayTip;

var startupDeferred;

var settings = {};

//For debugging purposes, we wrap the chrome API access here and hard code default settings.
try {
	settings.firstDayOfWeek = parseInt(safari.extension.settings.firstDayOfWeek);
	settings.showWeekNumber = parseInt(safari.extension.settings.showWeekNumber); 
}
catch(e)
{
	settings.firstDayOfWeek = 0;
	settings.showWeekNumber = 1;
}

var firstDayOfWeek = settings.firstDayOfWeek;
var showWeekNumber = settings.showWeekNumber;

//console.log(settings);

//Init today time stamp
var todayStamp = Date.UTC(now.getFullYear(),now.getMonth(), now.getDate());


/**
Bootstrap page on load
*/
$(document).ready(function() {
	
	try {
		initCalendarPageStart();	
	}
	catch(e)
	{
		handleError("Calendar, Documentready", e);
	}
	
});

/*
* First init
*/ 
function initCalendarPageStart()
{
	
	try {	
		initCalendarPage();	
	}
	catch (err) {
		handleError("Calendar, initCalendarPageStart", err);
	}
}

function initCalendarPage() {
	
	//Find first month to show. Defaults to January
	var startMonth = 1;	
	
	/*
	if(settings.showFrom == 3 && settings.popup == 12) { startMonth = currentMonth; }
	else if(settings.showFrom == 2 && settings.popup == 12) { startMonth = getStartMonthForQuarter(currentMonth);}
	*/
	
	initPopupPage(currentYear, startMonth);
	bindEvents();
	highLightToday();
	
}

/**
Bind all relevant events to their dom elements
*/
function bindEvents()
{
	
	try {
		
			//$("body").off().on("keydown", function() { keyPressed(window.event.keyCode);}); // .on("contextmenu", dayRightClicked);	

			//Make year links work
			$(".yearlink").off().on("click", function() { yearClicked(event); });

			document.addEventListener("mousewheel", MouseWheelHandler, false);
			
			safari.extension.settings.addEventListener("change", reload, false);
			
					
		}
		catch(e)
		{
			handleError("Calendar.js bindEvents" ,e);
	}
	
}

function reload()
{
	location.reload();
}

/**
Handle mouse wheel for scrolling
*/
function MouseWheelHandler(e)
{
	try {
		//Move threshold
		var threshold = 300;
		
		//Remove existing timeout
		clearTimeout(runningMouseWheelTimer);
		
		//Add new timeout
		runningMouseWheelTimer = window.setTimeout(mouseWheelReset, 400);
		
		//Get delta for this event
		var delta = e.wheelDelta;
		var isNegative = (delta < 0) ? true : false;
	
		//Add delta to current value
		runningMouseWheelCounter += delta;
		
		if(Math.abs(runningMouseWheelCounter) > threshold)
		{
			runningMouseWheelCounter = 0; //Reset
			
			if(!isNegative)
			{
				shiftCalendarByMonths(-4);
			}
			else {
				shiftCalendarByMonths(4);
			}
		}
		
		//log("Mousewheel", delta + " " + runningMouseWheelCounter);
		
		//trackEvent("Interaction", "Calendar", "Mouse wheel");
		
		return false;
	
	}
	catch(err)
	{
		handleError("Calendar.js MouseWheelHandler", err);
	}
	
}

/**
Reset mouse wheel counter
*/
function mouseWheelReset()
{
	//log("Mousewheel", "Resetting counter");
	runningMouseWheelCounter = 0;
}


/**
Initialize popup
 */
function initPopupPage(year, month)
{	
	try {
	
		//Set page title for tracking
		//document.title = version.currVersion;
	
		populateYearLinks();

		//Display the calendar
		showCal(year, month);
		
		//Show list if applicable
	//	populateListStyle();
	
		//Bind all events
		//bindEvents();
	
		
	}
	catch(e)
	{
		handleError("Calendar, initPopupPage", e);
	}
}

/**
Highlights the chosen dates, from loaded value 
*/
function Calendar(year, month)
{

	try {	
		
		//Functions
		this.getCal = calGetCal;
	
		//working variables
		this.year = year;
		this.month = month;
		this.workMonth = month-1;

		this.workDate = new Date(Date.UTC(year,this.workMonth,1));
		


		this.startStamp = Date.UTC(year,this.workMonth,1);
		
		this.outVars = {};

		//console.log(this);
	
	}
	catch(e)
	{
		handleError("Calendar.js Calendar", e);
	}
	
}	

var monthNames = ["Nulluary", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

/**
Return the actual calendar html
*/
function calGetCal(template)
{
	try {

		//Set month name
		this.outVars.monthName = monthNames[this.month]; //ucFirst(chrome.i18n.getMessage("mon"+this.month));
		
		this.outVars.year = this.year.toString().substring(0,4);
	
		//Set week header value
		this.outVars.weekShortName = "w";
	
		//Set day names
		if(firstDayOfWeek === 0) {
			this.outVars.day0_ShortName = "S"; //chrome.i18n.getMessage("sday0");
			this.outVars.day1_ShortName = "M"; //chrome.i18n.getMessage("sday1");
			this.outVars.day2_ShortName = "T"; //chrome.i18n.getMessage("sday2");
			this.outVars.day3_ShortName = "W"; //chrome.i18n.getMessage("sday3");
			this.outVars.day4_ShortName = "T"; //chrome.i18n.getMessage("sday4");
			this.outVars.day5_ShortName = "F"; //chrome.i18n.getMessage("sday5");
			this.outVars.day6_ShortName = "S"; //chrome.i18n.getMessage("sday6");
		}
		else {
			this.outVars.day0_ShortName = "M"; //chrome.i18n.getMessage("sday1");
			this.outVars.day1_ShortName = "T"; //chrome.i18n.getMessage("sday2");
			this.outVars.day2_ShortName = "W"; //chrome.i18n.getMessage("sday3");
			this.outVars.day3_ShortName = "T"; // chrome.i18n.getMessage("sday4");
			this.outVars.day4_ShortName = "F"; //chrome.i18n.getMessage("sday5");
			this.outVars.day5_ShortName = "S"; //chrome.i18n.getMessage("sday6");
			this.outVars.day6_ShortName = "S"; //hrome.i18n.getMessage("sday0");
		}
	
		var startWeekDay = this.workDate.getUTCDay();
		
		var tabWidth = 8;
		if(this.showWeekNumber === 0) tabWidth = 7;

		var templateOffsetDays = 0;
	
		//	Screw rules, hard code instead. This is Sunday first.
		if(firstDayOfWeek === 0)
		{
			templateOffsetDays = startWeekDay;
		}
		else //Monday
		{
			if(startWeekDay === 0)
			{
				templateOffsetDays = 6;
			}
			else
			{
				templateOffsetDays = startWeekDay - 1;
			}
		}

		var tmpDate;
		var currentTemplateDay = templateOffsetDays;
		var currentWeek = 0;

		//	The actual day adder code
		for(var i = 0; i < this.workDate.getDaysInMonth(); i++)
		{
			var dayStamp = this.startStamp + (i * 86400000);
			tmpDate = new Date(dayStamp);

			currentWeek = Math.floor((i+templateOffsetDays)/7);
			
			if(i === 0)
			{
				this.outVars["w_"+currentWeek] = tmpDate.getWeek();
			}
			else if(tmpDate.getUTCDay() === 4)
			{
				this.outVars["w_"+currentWeek] = tmpDate.getWeek();
			}
			else if(tmpDate.getDate() > (this.workDate.getDaysInMonth()-4))
			{
				this.outVars["w_"+currentWeek] = tmpDate.getWeek();
			}
	
			this.outVars["d_stamp_"+(i+templateOffsetDays)] = dayStamp;
			this.outVars["d_content_"+(i+templateOffsetDays)] = tmpDate.getDate();
			this.outVars["d_class_"+(i+templateOffsetDays)] = "cal_td_day";
			this.outVars["d_id_"+(i+templateOffsetDays)] = dayStamp;

		}
	
		var thisCalOutHtml = template;
	
		//New home made str_replace version
		var replaceString;
		
		$.each(this.outVars, function(key, value){
			
			replaceString = "${"+key+"}";
			
			thisCalOutHtml = thisCalOutHtml.replace(replaceString,value);
			
		});
			
		myregexp = new RegExp(/\${[a-zA-Z0-9_-]*}/gi);
		
		thisCalOutHtml = thisCalOutHtml.replace(myregexp, "");
		
		return thisCalOutHtml;
	}
	catch(e)
	{
		handleError("Calendar.js calgetCal", e);
	}
}

/**
Shift calendar by N months
*/
function shiftCalendarByMonths(deltaMonths)
{
	var tmpDate = new Date(showingFromYear, showingFromMonth-1+deltaMonths, 1);
	
	showCal(tmpDate.getFullYear(), tmpDate.getMonth()+1);
}

/**
Create a calendar
*/
function showCal(year, month)
{	
	try {

	
	//	firstDayOfWeek = settings.firstDayOfWeek * 1;
	
		populate12MonthsFrom(year, month, "month", monthTemplate); //this year from january
	
		populateYearLinks();

		if(showWeekNumber == 0) $(".cal_weekblock").hide();
		
	}
	catch(e)
	{
		handleError("Calendar.js showCal", e);
	}

}

function populate12MonthsFrom(year, month, selectstring, template)
{
	
	try {
	
	showingFromMonth = month;
	showingFromYear = year;

	for(var i = 1; i < 13; i++)
	{
		var selectString = "#"+selectstring;
		if(i<10) selectString += "0";
		selectString += i;
		
		var tmpDate = new Date(year, month-2+i, 1);
		
		var tyear = tmpDate.getFullYear();
		var tmonth = tmpDate.getMonth()+1;
		
		$(selectString).html(new Calendar(tyear,tmonth).getCal(template));
		
	}

	}
	catch(e)
	{
		handleError("Calendar.js populate12MonthsFrom", e);
	}	

}


/**
Add the links to the link bar
 */
function populateYearLinks()
{
	try {
		//Remove markings
		$(".yearlink").removeClass(yearButtonFullClass).removeClass(yearButtonHalfClass).removeClass(yearButtonThirdClass);
		
		//We are showing a full year
		if(showingFromMonth == 1)
		{
			baseYear = showingFromYear;
			$("#yearLabel").addClass(yearButtonFullClass);
			
		}
		else if(showingFromMonth < 7)
		{
			baseYear = showingFromYear;
			$("#yearLabel").addClass(yearButtonHalfClass);
			$("#yp1").addClass(yearButtonThirdClass);
			
		}
		else {
			baseYear = showingFromYear + 1;
			$("#yearLabel").addClass(yearButtonHalfClass);
			$("#ym1").addClass(yearButtonThirdClass);
		}
		
		$("#ym6").html(baseYear-6).attr("year", baseYear-6);
		$("#ym1").html(baseYear-1).attr("year", baseYear-1);
		$("#ym2").html(baseYear-2).attr("year", baseYear-2);
		$("#ym3").html(baseYear-3).attr("year", baseYear-3);
		$("#ym4").html(baseYear-4).attr("year", baseYear-4);
		$("#ym5").html(baseYear-5).attr("year", baseYear-5);
		$("#yearLabel").html(baseYear).attr("year", baseYear);
		$("#yp1").html(baseYear+1).attr("year", baseYear+1);
		$("#yp2").html(baseYear+2).attr("year", baseYear+2);
		$("#yp3").html(baseYear+3).attr("year", baseYear+3);
		$("#yp4").html(baseYear+4).attr("year", baseYear+4);
		$("#yp5").html(baseYear+5).attr("year", baseYear+5);
		$("#yp6").html(baseYear+6).attr("year", baseYear+6);
	}
	catch(e)
	{
		handleError("Calendar.js populateYearLinks", e);
	}
}

/**
Shift calendar by N months
*/
function shiftCalendarByMonths(deltaMonths)
{
	var tmpDate = new Date(showingFromYear, showingFromMonth-1+deltaMonths, 1);
	
	showCal(tmpDate.getFullYear(), tmpDate.getMonth()+1);
}

/**
The user has clicked a year link and we need to go to another year

@param offset Delta between clicked year and current view
 */
function yearClicked(event){

	try {
		var year = $(event.target).attr("year");
			
		currentYear = year*1; //Add offset to current year
	
		showCal(currentYear, 1);
		
	}
	catch(e)
	{
		handleError("Calendar.js yearClicked", e);
	}
	
}

function getStartMonthForQuarter(month)
{
	if(month < 5)
	{
		return 1;
	}
	else if(month < 9)
	{
		return 5;
	}
	else {
		return 9;
	}
}

/**
Highlight today
*/
function highLightToday()
{
	highLightDay(todayStamp, 'cal_day_today');
}

/**
Highlight a specific day, remove other hightlights
*/
function highLightDay(timestamp, highlightClass)
{	
	var selectorString = '[dateTimestamp="'+timestamp+'"]';
	$(selectorString).addClass(highlightClass);

}

function handleError(where, e)
{
    var time = new Date();
    console.error(time.toLocaleTimeString(), where, e.name, e.message);

}

